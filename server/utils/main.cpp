#include <QtGui/QApplication>
#include <QtCore/QTranslator>
#include "hstestcfg.h"


int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QTranslator qtTranslator;
#ifdef Q_OS_LINUX
    qtTranslator.load("qt_" + QLocale::system().name(), "/usr/share/qt4/translations");
#endif
#ifdef Q_OS_WIN32
    qtTranslator.load("qt_" + QLocale::system().name(), QApplication::applicationDirPath () + "/translations");
#endif
    app.installTranslator(&qtTranslator);

    hstestcfg foo;
    foo.show();
    return app.exec();
}
// kate: indent-mode cstyle; replace-tabs on; 
