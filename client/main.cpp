#include <QtGui/QApplication>
#include <QtCore/QTranslator>
#include "hstester-client.h"


int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    app.setApplicationName("Hs Test");
    app.setApplicationVersion("0.6.0");

    QTranslator qtTranslator;
#ifdef Q_OS_LINUX
    qtTranslator.load("qt_" + QLocale::system().name(), "/usr/share/qt4/translations");
#endif
#ifdef Q_OS_WIN32
    qtTranslator.load("qt_" + QLocale::system().name(), QApplication::applicationDirPath () + "/translations");
#endif
    app.installTranslator(&qtTranslator);

    MainWindow mw;
    mw.show();
    return app.exec();
}
