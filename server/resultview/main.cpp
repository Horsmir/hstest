#include <QtGui/QApplication>
#include <QtCore/QTranslator>
#include <QtCore/QLocale>
#include "resultview.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QTranslator qtTranslator;
#ifdef Q_OS_LINUX
    qtTranslator.load("qt_" + QLocale::system().name(), "/usr/share/qt4/translations");
#endif
#ifdef Q_OS_WIN32
    qtTranslator.load("qt_" + QLocale::system().name(), QApplication::applicationDirPath () + "/translations");
#endif
    app.installTranslator(&qtTranslator);

    ResultView foo;
    foo.show();
    return app.exec();
}
